﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipHandle : MonoBehaviour
{
    public UnityEngine.UI.Image img;
    public RectTransform controller;
    public bool right;

    private void Start() {
        right = false;
    }
    public void ChangeOrientation() {
        right = !right;
        if (right) {
            img.rectTransform.localScale = new Vector3(-1, 1, 1);
            controller.SetPositionAndRotation( new Vector3(1500, 0, 0), Quaternion.identity);
            
        } else {
            img.rectTransform.localScale = new Vector3(1, 1, 1);
            controller.position = Vector3.zero;
        }
    }
}
