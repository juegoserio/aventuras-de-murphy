﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private bool loading = false;

    public void ChangeScene(int index) {
        if (!loading) {
            SceneManager.LoadScene(index);
            //StartCoroutine(LoadAsyncScene(index));
            loading = true;
        }
    }
    public void ResetLoading() {
        loading = false;
    }
    IEnumerator LoadAsyncScene(int index) {
        AsyncOperation load = SceneManager.LoadSceneAsync(index);
        
        while (!load.isDone) {
            yield return null;
        }
    }
}
