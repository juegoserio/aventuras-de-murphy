﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	
	[SerializeField] private Transform player;
	private Vector3 tempPos;

	[SerializeField] private Vector3 offset;
	
	void Update () {
		tempPos = transform.position;

		tempPos = player.position + offset;
		
		transform.position = tempPos;
	}
}
