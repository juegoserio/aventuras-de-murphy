﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractiveController : MonoBehaviour
{
    public static InteractiveController instance;

    [SerializeField] private TMP_Text title;
    [SerializeField] private TMP_Text description;
    [SerializeField] private TMP_Text warning;
    [SerializeField] private TMP_Text btnInfo;
    [SerializeField] private UnityEngine.UI.Image image;
    [SerializeField] private ObjectiveInteractable[] interactables;

    [SerializeField] private UnityEngine.UI.Slider sld;
    [SerializeField] private UnityEngine.UI.Button btn;
    [SerializeField] private UnityEngine.UI.Image turdonImage;
    [SerializeField] private Sprite[] turdons;
    [SerializeField] private Animator anim;
    [SerializeField] private Animator turdonOrangeAnim;
    [SerializeField] private GameObject panel;
    [SerializeField] private GameObject interactablePanel;
    private int current;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }
    private void Start() {
        //El metodo clear limpia todo el contenido que tenga el HUD hasta ese momento y monta el primer mensaje.
        //ClearInfo();

        current = 0;
        anim.gameObject.SetActive(false);
        sld.gameObject.SetActive(false);
        turdonImage.gameObject.SetActive(false);

        interactablePanel.SetActive(false);
    }

    private void SetObjectiveInfo(ObjectiveInfo info, string btnMsj) {
        title.text = info.title;
        description.text = info.description;
        warning.text = info.warning;
        image.sprite = info.image;
        btnInfo.text = btnMsj;
    }

    private void SetDescription(string _desc) {
        description.text = _desc;
    }

    private void SetWarnings(string _warning) {
        warning.text = _warning;
    }
    public void ClearInfo() {
        Debug.Log("Llamado de clear info");
        panel.SetActive(true);
        interactablePanel.SetActive(true);
        
        SetObjectiveInfo(interactables[0].info, "INICIAR");
    }
    public void IterateInteractable() {
        switch (current) {
            case 0:
                current++;
                SetObjectiveInfo(interactables[1].info, "CONTINUAR");
                break;
            case 1:
                anim.gameObject.SetActive(true);
                sld.gameObject.SetActive(true);
                SetObjectiveInfo(interactables[2].info, "COMPROBAR");
                StartCoroutine(RefreshWarnings(1.5f));
                break;
            case 2:
                current++;
                break;
            case 3:
                turdonImage.sprite = turdons[1];
                turdonImage.rectTransform.localScale = new Vector3(0.6f,0.6f,0.6f);
                SetObjectiveInfo(interactables[4].info, "CONTINUAR");
                current++;
                break;
            case 4:
                anim.gameObject.SetActive(false);
                turdonImage.gameObject.SetActive(false);
                current++;
                SetObjectiveInfo(interactables[5].info, "CONTINUAR");
                break;
            case 5:
                interactablePanel.SetActive(false);
                panel.SetActive(false);
                GameManager.instance.CheckStateAndAdvance();
                break;
        }
    }

    IEnumerator WaitAndAnimate() {
        if (anim != null) {
            anim.SetBool("Quiet", sld.value > 0.46f && sld.value < 0.54f);
        }
        btn.interactable = false;
        yield return new WaitForSeconds(0.6f);
    }

    IEnumerator RefreshWarnings(float t) {
        StartCoroutine(WaitAndAnimate());
        if (sld.value > 0.46f && sld.value < 0.54f) {
            SetDescription("Perfecto, ahora puedes saber el valor exacto de la masa de los turdones.");
            SetWarnings("Excelente, la balanza esta correctamente configurada");
        }
        else {
            SetWarnings("Lo sentimos, la balanza aun no da los valores adecuados");
        }
        yield return new WaitForSeconds(t);
        SetWarnings(" ");
        btn.interactable = true;
        if (sld.value > 0.46f && sld.value < 0.54f) {
            sld.gameObject.SetActive(false);
            anim.speed = 0;
            current++;
            SetObjectiveInfo(interactables[3].info, "CONTINUAR");
            SetWarnings(" ");
            StartCoroutine(OrangeTurdonAnimation());
        }
    }
    IEnumerator OrangeTurdonAnimation() {
        turdonImage.gameObject.SetActive(true);
        turdonImage.rectTransform.localScale = Vector3.one;
        turdonImage.sprite = turdons[0];
        btn.interactable = false;
        yield return new WaitForSeconds(2.0f);
        SetObjectiveInfo(interactables[3].info, "CONTINUAR");
        btn.interactable = true;        
    }
}
