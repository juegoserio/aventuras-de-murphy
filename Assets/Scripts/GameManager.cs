﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { 
    Intro,    
    TriggerToDialCollect, DialogueCollect, 
    CollectingChall, PopUpEndCollect,
    TriggerScale, DiallogueScale, 
    ScaleChall, EndScaleChall,
    TriggerTrivia, TriviaChall, 
    TriggerMision2, DialogueMision2, TriggerTriviaM2, 
    TriggerMision3, DialogueMision3, TriggerTriviaM3,
    TriggerMision4, DialogueMision4, TriggerTriviaM4,
    TriggerMision5, DialogueMision5, TriggerTriviaM5,
    End }

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [Header("Triggers Setup")]
    [SerializeField] private GameObject[] triggerElements;
    [SerializeField] private DialogueTrigger[] dialogueTriggers;

    [Header("Turdon Collectable Info")]
    private int turdonOrange;
    private int turdonGreen;
    private int tOrangeCollected;
    private int tGreenCollected;
    
    [SerializeField] private Transform tOrangePref;
    [SerializeField] private Transform tGreenPref;
    [SerializeField] private Transform[] tOrangePos;
    [SerializeField] private Transform[] tGreenPos;
    [SerializeField] private InteractiveController interactiveController;

    [HideInInspector] public float timeSeconds;

    private SceneController sc;

    public GameState state;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    private void Start() {
        //state = GameState.DiallogueScale;
        state = GameState.Intro;
        TurdonSpawner(tOrangePref, tOrangePos);
        TurdonSpawner(tGreenPref, tGreenPos);
        for(int i=0; i < triggerElements.Length; i++) {
            triggerElements[i].SetActive(false);
        }
        CheckStateAndAdvance();
    }

    private IEnumerator ResetSettingsMission1(float t) {
        
        yield return new WaitForSeconds(t);

        tOrangeCollected = 0;
        tGreenCollected = 0;
        timeSeconds = 119.0f;
        SetTurdonGoal(8, 5);
        ActivateTurdonCollectable(tOrangePos);
        ActivateTurdonCollectable(tGreenPos);
        HUDManager.instance.SetTimerInfo(timeSeconds);
        HUDManager.instance.SetInicialCollectingChallengeText(turdonOrange, tOrangeCollected, turdonGreen, tGreenCollected);        
    }

    public void SetTurdonCollected(int orange, int green) {
        tOrangeCollected = orange;
        tGreenCollected = green;
    }

    private void SetTurdonGoal(int orange, int green) {
        turdonOrange = orange;
        turdonGreen = green;
    }
    public void TurdonHide() {
        for (int i = 0; i < tOrangePos.Length; i++) {
            tOrangePos[i].gameObject.SetActive(false);
        }
        for (int j=0; j < tGreenPos.Length; j++) {
            tGreenPos[j].gameObject.SetActive(false);
        }
    }
    private void TurdonSpawner(Transform prefab, Transform[] pos) {
        for (int i = 0; i < pos.Length; i++) {
            Instantiate(prefab, pos[i].position, Quaternion.Euler(-90.0f,0,0),pos[i]);
            prefab.gameObject.SetActive(false);
        }
    }
    private void ActivateTurdonCollectable(Transform[] prefabs) {
        for (int i = 0; i < prefabs.Length; i++) {
            prefabs[i].GetChild(0).gameObject.SetActive(true);
            prefabs[i].GetChild(0).GetComponent<TurdonCollectable>().ResetCollectable();
        }
    }

    public void IncreaseTurdonCollected(string param) {
        if (param == "Green" && tGreenCollected < turdonGreen) {
            tGreenCollected++;
        }
        if (param == "Orange" && tOrangeCollected < turdonOrange) {
            tOrangeCollected++;
        }
    }
    public string GetTurdonPurpleCollected() {
        return tGreenCollected.ToString();
    }
    public string GetTurdonOrangeCollected() {
        return tOrangeCollected.ToString();
    }
    public bool CheckCollected() {
        return tOrangeCollected == turdonOrange && tOrangeCollected > 0 && tGreenCollected == turdonGreen && tOrangeCollected > 1 && tGreenCollected > 1;
    }
    public string GetCollectedTurdons() {
        if (CheckCollected()) {
            return  "¡Exelente! Has recolectado todos los turdones. Ve al punto indicado en el mapa para continuar tu misión";
        }
        else {
            int restOrange = turdonOrange - tOrangeCollected;
            int restPurple = turdonGreen - tGreenCollected;
            return "El tiempo se acabó, debes intentar nuevamente ya que te faltaron " + restOrange + " turdones Grandes y " + restPurple + " turdones pequeños.";            
        }
    }
    public void CheckStateAndAdvance() {
        switch (state) {
            case GameState.Intro:
                Debug.Log("introooo");
                triggerElements[0].SetActive(true);
                break;
            case GameState.TriggerToDialCollect:
                dialogueTriggers[0].TriggerDialogue();
                state = GameState.DialogueCollect;
                break;
            case GameState.DialogueCollect:
                StartCoroutine(ResetSettingsMission1(1.0f));
                state = GameState.CollectingChall;                
                break;
            case GameState.CollectingChall:
                if (CheckCollected()) {
                    state = GameState.PopUpEndCollect;
                    triggerElements[1].SetActive(true);
                } else {
                    StartCoroutine(ResetSettingsMission1(1.0f));
                }
                break;
            case GameState.PopUpEndCollect:
                state = GameState.TriggerScale;
                break;
            case GameState.TriggerScale:
                state = GameState.DiallogueScale;
                triggerElements[2].SetActive(true);
                break;
            case GameState.DiallogueScale:
                state = GameState.ScaleChall;
                interactiveController.ClearInfo();
                break;
            case GameState.ScaleChall:
                state = GameState.TriggerTrivia;
                Debug.Log("trivia triggger");
                triggerElements[3].SetActive(true);
                break;
            case GameState.TriggerTrivia:
                Debug.Log("trivia reto");
                TriviaController.instance.StartTrivia(1);
                state = GameState.TriviaChall;
                break;
            case GameState.TriviaChall:
                Debug.Log("Trigger a dialogo m2");
                dialogueTriggers[1].TriggerDialogue();
                state = GameState.TriggerMision2;
                break;
            case GameState.TriggerMision2:
                triggerElements[4].SetActive(true);
                Debug.Log("Dialogo M2");
                state = GameState.DialogueMision2;
                break;
            case GameState.DialogueMision2:
                TriviaController.instance.StartTrivia(2);
                Debug.Log("A - Mision y se activa el trigger a la trivia");
                state = GameState.TriggerTriviaM2;
                break;
            case GameState.TriggerTriviaM2:
                dialogueTriggers[2].TriggerDialogue();
                state = GameState.TriggerMision3;
                Debug.Log("B - empezar trivia 2");
                break;
            case GameState.TriggerMision3:
                triggerElements[5].SetActive(true);
                Debug.Log("Dialogo M3");
                state = GameState.DialogueMision3;
                break;
            case GameState.DialogueMision3:
                TriviaController.instance.StartTrivia(3);
                Debug.Log("C - Mision y se activa el trigger a la trivia3");
                state = GameState.TriggerTriviaM3;
                break;
            case GameState.TriggerTriviaM3:
                dialogueTriggers[3].TriggerDialogue();
                state = GameState.TriggerMision4;
                Debug.Log("D - empezar trivia 4");
                break;
            case GameState.TriggerMision4:
                triggerElements[6].SetActive(true);
                Debug.Log("Dialogo M4");
                state = GameState.DialogueMision4;
                break;
            case GameState.DialogueMision4:
                TriviaController.instance.StartTrivia(4);
                Debug.Log("E - Mision y se activa el trigger a la trivia4");
                state = GameState.TriggerTriviaM4;
                break;
            case GameState.TriggerTriviaM4:
                dialogueTriggers[4].TriggerDialogue();
                state = GameState.TriggerMision5;
                Debug.Log("F - empezar trivia 5");
                break;
            case GameState.TriggerMision5:
                triggerElements[7].SetActive(true);
                Debug.Log("Dialogo M5");
                state = GameState.DialogueMision5;
                break;
            case GameState.DialogueMision5:
                TriviaController.instance.StartTrivia(5);
                Debug.Log("A - Mision y se activa el trigger a la trivia5");
                state = GameState.TriggerTriviaM5;
                break;
            case GameState.TriggerTriviaM5:
                dialogueTriggers[5].TriggerDialogue();
                state = GameState.End;
                Debug.Log("Fin y creditos");
                break;
            case GameState.End:
                sc.ChangeScene(0);
                break;
        }
    }
}
