﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurdonCollectable : MonoBehaviour
{
    [SerializeField] private string param;
    private AudioSource tAudio;
    private bool counted;
    private void Start() {
        tAudio = GetComponent<AudioSource>();
        tAudio.loop = false;
        tAudio.playOnAwake = false;
        ResetCollectable();
    }

    public void ResetCollectable() {
        counted = false;        
    }
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player") && !counted) {
            StartCoroutine(HideAndCount());
            counted = true;
        }
    }
    IEnumerator HideAndCount() {
        tAudio.Play();
        GameManager.instance.IncreaseTurdonCollected(param);        
        yield return new WaitForSeconds(1.0f);
        if (GameManager.instance.CheckCollected()) {
            //GameManager.instance.CheckStateAndAdvance();
            HUDManager.instance.PopUpCollectable();
        }
        gameObject.SetActive(false);
    }
}
