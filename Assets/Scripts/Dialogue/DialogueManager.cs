﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager instance;

    public AudioSource typeSound;
    public TMP_Text nameText;
    public TMP_Text dialogueText;
    private Queue<BoxChat> sentences;
    
    [SerializeField] private Image charImg;
    [SerializeField] private Image dialImg;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject panel;
    [SerializeField] private Button btn;

    private bool isOver;
    
    private void Awake() {
        if(instance == null){
            instance = this;
        }
    }
    void Start()
    {
        sentences = new Queue<BoxChat>();
    }
    
    public void StartDialogue(Dialogue dialogue)
    {
        isOver = false;
        anim.SetBool("Close", false);
        sentences.Clear();
        foreach (BoxChat sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        btn.interactable = true;
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (btn.interactable) {
            //Agregar tiempo de espera para que se tengan que mamar toda la lectura de esto con una corrutina o un if que espere hasta que este completo.
            if (sentences.Count == 0) {
                StartCoroutine(EndDialogue());
                return;
            }
            BoxChat sentence = sentences.Dequeue();

            nameText.text = sentence.name;
            charImg.sprite = sentence.characterImage != null ? sentence.characterImage : null;
            dialImg.sprite = sentence.dialogueImage != null ? sentence.dialogueImage : null;

            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence.sentence));
        }
    }

    IEnumerator TypeSentence (string sentence)
    {
        btn.interactable = false;
        typeSound.Play();
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
        typeSound.Stop();
        btn.interactable = true;
    }
    IEnumerator EndDialogue()
    {
        anim.SetBool("Close", true);
        isOver = true;
        yield return new WaitForSeconds(1.4f);
        if (panel != null) {
            panel.SetActive(true);
        }
    }

    public bool ConversationIsOver() {
        return isOver;
    }
}
