﻿using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public BoxChat[] sentences;
}

[System.Serializable]
public struct BoxChat
{
    public string name;
    public Sprite characterImage;
    public Sprite dialogueImage;
    [TextArea(3, 10)]
    public string sentence;    
}

[System.Serializable]
public class ObjectiveInfo
{
    public Sprite image;
    public string title;
    [TextArea(3, 10)]
    public string description;
    public string warning;
}
public enum InteractableState { Other, SliderCheck, End}

[System.Serializable]
public struct ObjectiveInteractable
{
    public ObjectiveInfo info;
    public bool sliderCheck;
    [HideInInspector] public string btn;
}

