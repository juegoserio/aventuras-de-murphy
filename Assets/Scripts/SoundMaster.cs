﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMaster : MonoBehaviour
{
    public AudioSource myAudio;
    public UnityEngine.UI.Image image;
    public Sprite[] silenced;
    private void Start() {
        myAudio.loop = true;
        myAudio.playOnAwake = true;
    }

    public void MuteOrSound() {
        myAudio.mute = !myAudio.mute;
        image.sprite = myAudio.mute ? silenced[0] : silenced[1];
    }
}
