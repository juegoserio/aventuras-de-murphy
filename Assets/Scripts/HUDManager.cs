﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUDManager : MonoBehaviour
{
    public static HUDManager instance;
    [Header("Objective Settings")]
    [SerializeField] private TMP_Text objTitle;
    [SerializeField] private TMP_Text objDescription;
    [SerializeField] private TMP_Text objWarning;
    [SerializeField] private TMP_Text objBtn;
    [SerializeField] private Image objImage;

    [SerializeField] private ObjectiveInfo[] objInfo;
    [SerializeField] private GameObject objPanel;
    
    [Header("Mission 1 Settings")]
    [SerializeField] private GameObject MsjDialoguePanel;
    [SerializeField] private TMP_Text tOrangeGoal; // Turdon Orange Goal
    [SerializeField] private TMP_Text tOrangeCollected; // Turdon Orange Goal
    [SerializeField] private TMP_Text tGreenGoal; // Turdon Purple Goal
    [SerializeField] private TMP_Text tGreenCollected; // Turdon Purple Goal
    [SerializeField] private TMP_Text timer;
    [SerializeField] private GameObject collectablePanel;

    [SerializeField] private GameObject popUpInfo;
    [SerializeField] private TMP_Text popUpTxt;
    [SerializeField] private TMP_Text popUpBtnTxt;

    private float tempTimer;
    private float timerF;
    private bool timerCheck;
    [SerializeField] private bool showOnce;

    private void Awake() {
        if(instance == null) {
            instance = this;
        }
    }
    private void Start() {
        showOnce = false;
        SetObjectiveInfo(0);
        SetTimerInfo(10);
        MsjDialoguePanel.SetActive(false);
        popUpInfo.SetActive(false);
        collectablePanel.SetActive(false);
        HideInteractablePanel();
        objPanel.SetActive(true);
    }

    private void SetUpDialogue() {
        if (MsjDialoguePanel.activeInHierarchy) {
            MsjDialoguePanel.SetActive(false);
        }
        if (objPanel.activeInHierarchy) {
            objPanel.SetActive(false);
        }
    }

    private void SetUpTrigger(int index) {
        if (!MsjDialoguePanel.activeInHierarchy) {
            MsjDialoguePanel.SetActive(true);
            showOnce = false;
        }
        if (DialogueManager.instance.ConversationIsOver()) {
            SetObjectiveInfo(index);
        }
    }
    private void Update() {
        switch (GameManager.instance.state) {
            case GameState.DialogueCollect:
                if (!MsjDialoguePanel.activeInHierarchy) {
                    MsjDialoguePanel.SetActive(true);
                    showOnce = false;
                }
                if (DialogueManager.instance.ConversationIsOver()) {
                    SetObjectiveInfo(1);
                }
            break;
            case GameState.CollectingChall:
                if (!collectablePanel.activeInHierarchy) {
                    collectablePanel.SetActive(true);
                }
                UpdateCollectingChallengeHUD();
                if (!GameManager.instance.CheckCollected()) {
                    FormatTimerText();                    
                }
            break;
            case GameState.PopUpEndCollect:
                if (collectablePanel.activeInHierarchy) {
                    collectablePanel.SetActive(false);
                }
            break;
            case GameState.TriggerScale:
                showOnce = false;
                SetObjectiveInfo(2);
            break;
            case GameState.TriggerMision2:
                SetUpTrigger(3);
                break;
            case GameState.DialogueMision2:
                SetUpDialogue();
                break;
            case GameState.TriggerMision3:
                SetUpTrigger(4);
                break;
            case GameState.DialogueMision3:
                SetUpDialogue();
                break;
            case GameState.TriggerMision4:
                SetUpTrigger(5);
                break;
            case GameState.DialogueMision4:
                SetUpDialogue();
                break;
            case GameState.TriggerMision5:
                SetUpTrigger(6);
                break;
            case GameState.DialogueMision5:
                SetUpDialogue();
                break;
        }        
        if (Input.GetKeyDown(KeyCode.A)) {
            GameManager.instance.SetTurdonCollected(8, 5);
        }
        if (Input.GetKeyDown(KeyCode.Q)) {
            GameManager.instance.TurdonHide();
        }
    }    
    public void SetObjectiveInfo(int index) {
        if (!objPanel.activeInHierarchy) {
            objPanel.SetActive(true);
        }
        if (!showOnce) {
            objTitle.text = objInfo[index].title;
            objDescription.text = objInfo[index].description;
            objWarning.text = objInfo[index].warning;
            objImage.sprite = objInfo[index].image;
            showOnce = true;
        }
    }

    public void SetObjectiveInfoExternal(ObjectiveInfo info, string btnTxt) {
        objTitle.text = info.title;
        objDescription.text = info.description;
        objWarning.text = info.warning;
        objImage.sprite = info.image;
        objBtn.text = btnTxt;
        //showOnce = true;
    }
    public void SetInicialCollectingChallengeText(int tOrGoal, int tOrColl, int tPurGoal, int tPurColl ) {
        tOrangeGoal.text = tOrGoal.ToString();
        tOrangeCollected.text = tOrColl.ToString() + "/";
        tGreenGoal.text = tPurGoal.ToString();
        tGreenCollected.text = tPurColl.ToString() + "/";
    }
    public void HideInteractablePanel() {
        if (objPanel.activeInHierarchy ) {            
            objPanel.SetActive(false);
        } 
    }
    public void SetTimerInfo(float t) {
        timerF = t;
        timerCheck = false;
    }

    public void PopUpCollectable() {
        popUpInfo.SetActive(true);
        popUpTxt.text = GameManager.instance.GetCollectedTurdons();
    }

    private void UpdateCollectingChallengeHUD() {
        tOrangeCollected.text = GameManager.instance.GetTurdonOrangeCollected() + "/";
        tGreenCollected.text = GameManager.instance.GetTurdonPurpleCollected() + "/";
    }

    private void FormatTimerText() {
        timerF -= Time.deltaTime;
        if (timerF > 69.5f && timerF< 120) {
            tempTimer = timerF - 60;
            timer.text = "01:" + tempTimer.ToString("F0"); ;
        }
        else if (timerF > 59.5f && timerF < 69.5f) {
            tempTimer = timerF - 60;
            timer.text = "01:0" + tempTimer.ToString("F0"); ;
        } 
        else if (timerF > 9.5f && timerF < 59.5f) {
            timer.text = "00:" + timerF.ToString("F0");
        } 
        else if (timerF > 0 && timerF < 9.5f) {
            timer.text = "00:0" + timerF.ToString("F0");
        } 
        if(timerF < 0.0f && !timerCheck) {
            timerCheck = true;
            PopUpCollectable();
        }
    }
}