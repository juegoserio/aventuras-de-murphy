﻿using System.Collections.Generic;

[System.Serializable]
public class Question
{
    public string statement;
    public List<string> options;
    public int rightChoise;
    public Question(string _statement, List<string> _options, int _rightChoise) {
        statement = _statement;
        options = _options;
        rightChoise = _rightChoise;
    }

    public void Shuffle(List<string> sentenceList) {
        System.Random random = new System.Random();
        for (int i = 0; i < sentenceList.Count - 1 ; i++) {
            int swapIndex = random.Next(sentenceList.Count);
            string temp = sentenceList[i];
            sentenceList[i] = sentenceList[swapIndex];
            sentenceList[swapIndex] = temp;
        }        
    }

    public void UpdateIndexChoise(List<string> sentenceList, string choise) {
        for(int i = 0; i < sentenceList.Count; i++) {
            if(sentenceList[i] == choise) {
                rightChoise = i;
            }
        }
    }

}
