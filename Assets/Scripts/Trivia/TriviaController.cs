﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class TriviaController : MonoBehaviour
{
    [SerializeField] private GameObject triviaGeneralPanel;
    [SerializeField] private List<Question> trivias = new List<Question>(); 
    private List<string> options;

    [SerializeField] private TMP_Text[] options_txt;
    [SerializeField] private TMP_Text sentence_txt;
    [SerializeField] private ToggleGroup toggleGroup;
    [SerializeField] private Toggle[] toggles;

    [SerializeField] private GameObject panelIntro;
    [SerializeField] private GameObject panelQuestions;
    [SerializeField] private GameObject panelResult;

    [SerializeField] private TMP_Text result_txt;
    [SerializeField] private Sprite[] result_sprites;
    [SerializeField] private Image result_img;
    [SerializeField] private TMP_Text btnResult_txt;

    [SerializeField] private TMP_Text intro_txt;


    private int actualQuestion;
    private int actualIndex;
    
    public int score;
    public static TriviaController instance;


    private void Awake() {
        if(instance == null) {
            instance = this;
        }
    }
    private void Start() {        
        triviaGeneralPanel.SetActive(false);        
    }
    public void StartTrivia(int index) {
        if (!triviaGeneralPanel.activeInHierarchy) {
            triviaGeneralPanel.SetActive(true);
        }
        GenerateTrivia(index);
        ResetTrivia();
        IntroPanel(index);
        actualIndex = index;        
    }
    private void ResetTrivia() {
        panelIntro.SetActive(true);
        panelQuestions.SetActive(false);
        panelResult.SetActive(false);
        actualQuestion = 0;
        UpdateHUD(trivias[actualQuestion]);
        score = 0;
    }
    private void ShuffleAndUpdate(int index, string ans) {
        trivias[index].Shuffle(trivias[index].options);
        trivias[index].UpdateIndexChoise(trivias[index].options, ans);
    }

    private void GenerateTrivia(int index) {
        switch (index) {
            case 1:
                trivias.Clear();

                trivias.Add(new Question("Si Murphy colocó en su mochila 3 turdones pequeños y 2 medianos, ¿la masa adicional en su espalda es?:", new List<string> { "900g", "500g", "700g", "400g" }, 0));
                ShuffleAndUpdate(0, "900g");

                trivias.Add(new Question("Si en la balanza Morphy coloca 5 turdones pequeños, ¿la lectura correcta será?:", new List<string> { "1500kg", "1,5kg", "150kg", "15kg" }, 0));
                ShuffleAndUpdate(1, "1,5kg");

                trivias.Add(new Question("Murphy recolectó 7 turdones medianos, ¿la masa que representan es?:", new List<string> { "2,1kg", "700g", "21kg", "0,21kg" }, 0));
                ShuffleAndUpdate(2, "2,1kg");

                trivias.Add(new Question("Si Morphy recolecta 3 turdones pequeños y 1 turdón mediano,  ¿el pago adecuado por esta transacción debe ser?:", new List<string> { "4 tokens", "5 tokens", "6 tokens", "7 tokens" }, 2));
                ShuffleAndUpdate(3, "6 tokens");

                trivias.Add(new Question("¿Cuántos kilogramos de turdones se pueden comprar con 9 tokens?", new List<string> { "0,9kg", "900kg", "9kg", "90kg" }, 0));
                ShuffleAndUpdate(4, "0,9kg");                
                break;
            case 2:
                trivias.Clear();

                trivias.Add(new Question("Murphy haciendo uso del rodatape calculará la distancia recorrida al transcurrir 30 minutos de viaje por la carretera, ayúdalo con este cálculo, teniendo en cuenta que la velocidad es de 12m/s, ¿la distancia recorrida es?:", new List<string> { "360 metros", "21.600 metros", "10.800 metros", "19.800 metros" }, 1));
                ShuffleAndUpdate(0, "21.600 metros");

                trivias.Add(new Question("¿cuantós segundos son necesarios para transportar turdones de la ciudad a la granja 1, si la velocidad es 12m/s?:", new List<string> { "59s", "291s", "12s", "5,8s" }, 0));
                ShuffleAndUpdate(1, "59s");

                trivias.Add(new Question("Morphy le indicó a los Unkas que debían cobrar más por el transporte de los turdones grandes, ¿Cuál es el pago justo que debe pagarse por el transporte de turdones transportados desde la ciudad hasta la granja 2?, teniendo cuenta la equivalencia entre el pago establecido en token y las distancias", new List<string> { "10 tokens", "20 tokens", "30 tokens", "50 tokens" }, 3));
                ShuffleAndUpdate(2, "50 tokens");

                trivias.Add(new Question("Teniendo en cuenta la equivalencia enunciada entre los token azules y verdes, el pago de 10 token azules, representa la siguiente cantidad de tokens verdes:", new List<string> { "10 verdes", "5 verdes", "2 verdes", "20 verdes" }, 2));
                ShuffleAndUpdate(3, "2 verdes");

                trivias.Add(new Question("Si en 30 minutos se han recorrido 21600 metros, cuántos token se pueden recibir haciendo entrega efectiva de turdones", new List<string> { "300", "150", "750", "1500" }, 1));
                ShuffleAndUpdate(4, "150");
                break;
            case 3:
                trivias.Clear();

                trivias.Add(new Question("Para recorrer una distancia de 1000 m, en el menor tiempo posible, es más recomendable para los Unkas:", new List<string> { "Utilizar el automóvil el cual se desplaza a una velocidad de 27.05 Km/h", "Utilizar la camioneta la cual se desplaza a una velocidad de 7.9 m/s", "Utilizar el automóvil si su velocidad es de 750 cm/s", "Utilizar la camioneta si su velocidad es de 45023,1 cm/minuto" }, 1));
                ShuffleAndUpdate(0, "Utilizar la camioneta la cual se desplaza a una velocidad de 7.9 m/s");

                trivias.Add(new Question("Para desplazarse de la granja de cultivo a la granja donde se encuentra el suministro de agua la cual dista 3,001 Km, dos Unkas (1 y 2) toman diferentes caminos (pero la distancia es igual) cual de los dos llegará primeros si:", new List<string> { "El Unka 1 se desplaza a velocidad constante de 25 Km/h", "El Unka 2 se desplaza a velocidad constante de 7.1 m/s", "Los dos Unkas llegan al mismo tiempo", "Es imposible que lleguen" }, 1));
                ShuffleAndUpdate(1, "El Unka 2 se desplaza a velocidad constante de 7.1 m/s");

                trivias.Add(new Question("Para transportar los 40 Kg de Turdones en el automóvil que tiene una capacidad de 16000 g, se debe realizar en:", new List<string> { "35 viajes", "30 viajes", "25 viajes", "20 viajes" }, 2));
                ShuffleAndUpdate(2, "25 viajes");

                trivias.Add(new Question("Los Unkas transportar 1.000.000 g de Turdones en la camioneta que tiene una capacidad de 40 Kg, para ello deben realizar:", new List<string> { "25 viajes", "15 viajes", "10 viajes", "30 viajes" }, 0));
                ShuffleAndUpdate(3, "25 viajes");

                trivias.Add(new Question("Si Morphy se desplaza en automóvil a una velocidad de 90 Km/h y desea detenerse en 10 s, la aceleración que debe aplicar es de:", new List<string> { "-2,5 m/s2", "9 m/s2", "2,5 m/s2", "-9 m/s2" }, 0));
                ShuffleAndUpdate(4, "-2,5 m/s2");
                break;
            case 4:
                trivias.Clear();

                trivias.Add(new Question("Al llegar a una nueva granja, Morphy se encuentra con un tanque rectangular que tiene las siguientes dimensiones: largo 2 m, alto 2 m y ancho 1 m, el volumen del tanque es:", new List<string> { "4 m3", "5 m3", "2m3", "6 m3" }, 0));
                ShuffleAndUpdate(0, "4 m3");

                trivias.Add(new Question("Si Morphy determina que el volumen de un tanque es 8 m3, sus dimensiones son:", new List<string> { "2m * 2m * 2m", "8m * 1m", "4m *1m *1m", "8m * 2m * 1m" }, 0));
                ShuffleAndUpdate(1, "2m * 2m * 2m");

                trivias.Add(new Question("Morphy quiere determinar el volumen de un tanque cilíndrico que tiene un diámetro de 2 m y 5 m de altura, los datos que debe colocar en la fórmula son: ", new List<string> { "π * (1 m)^2 * 5 m", "π * (2 m)^2 * 5 m", "π * 2 m * (5 m)^2", "π * 1 m * 5 m" }, 0));
                ShuffleAndUpdate(2, "π * (1 m)^2 * 5 m");

                trivias.Add(new Question("Si Morphy se encuentra con un tanque cilíndrico que tiene un diámetro de 2 m y una altura de 5 m, el volumen que debe reportar a los Nates es:", new List<string> { "10 π m3", "5 π m3", "50 π m3", "2 π m3" }, 0));
                ShuffleAndUpdate(3, "5 π m3");

                trivias.Add(new Question("Si Morphy tiene un tanque rectangular de 1 m por cada lado y otro cilíndrico de 1 m de altura y 1 m de radio, la afirmación correcta es:", new List<string> { "El tanque cilíndrico es más grande porque 1 π m3 es mayor a 1 m3", "El tanque rectangular es más pequeño porque 1 π m3 es menor a 1 m3", "El tanque cilíndrico es más pequeño porque 1 π m3 es mayor a 1 m3", "Los tanques son iguales porque 1 π m3 es igual a 1 m3" }, 0));
                ShuffleAndUpdate(4, "El tanque cilíndrico es más grande porque 1 π m3 es mayor a 1 m3");
                break;
            case 5:
                trivias.Clear();

                trivias.Add(new Question("Morphy desea conocer cual de los siguientes depósitos de forma rectangular puede almacenar mayor cantidad de agua. Depósito 1 Alto: 1.5 m; Largo: 5.0 m; Ancho: 4.0 m; Depósito 2  Alto: 2 m; Largo: 5 m; Ancho: 3 m; Depósito A Alto: 15 dm; Largo: 50 dm; Ancho: 40 dm y el Depósito B  Alto: 20 dm; Largo: 50 dm; Ancho: 4 dm", new List<string> { "El Depósito 1", "El Depósito 2", "El Depósito A", "El Depósito B" }, 3));
                ShuffleAndUpdate(0, "El Depósito B");

                trivias.Add(new Question("Morphy requiere conocer la equivalencia de las carretillas elevadoras en ergios, cuya energía es de 2,35x106 J", new List<string> { "2,35x10^12 e", "2,35x10^16 e", "2,35x10^1 e", "2,35x10^13 e" }, 3));
                ShuffleAndUpdate(1, "2,35x10^13 e");
                
                trivias.Add(new Question("Morphy desea conocer cual de los siguientes depósitos de forma rectangular puede almacenar mayor cantidad de agua. Depósito 1 Alto: 1.5 m; Largo: 5.0 m; Ancho: 4.0 m; Depósito 2  Alto: 2 m; Largo: 5 m; Ancho: 3 m; Depósito A Alto: 15 dm; Largo: 50 dm; Ancho: 40 dm y el Depósito B  Alto: 20 dm; Largo: 50 dm; Ancho: 4 dm", new List<string> { "El Depósito 1", "El Depósito 2", "El Depósito A", "El Depósito B" }, 3));
                ShuffleAndUpdate(2, "El Depósito B");

                trivias.Add(new Question("Morphy requiere conocer la equivalencia de las carretillas elevadoras en ergios, cuya energía es de 2,35x106 J", new List<string> { "2,35x10^12 e", "2,35x10^16 e", "2,35x10^1 e", "2,35x10^13 e" }, 3));
                ShuffleAndUpdate(3, "2,35x10^13 e");

                trivias.Add(new Question("Morphy desea conocer cual de los siguientes depósitos de forma rectangular puede almacenar mayor cantidad de agua. Depósito 1 Alto: 1.5 m; Largo: 5.0 m; Ancho: 4.0 m; Depósito 2  Alto: 2 m; Largo: 5 m; Ancho: 3 m; Depósito A Alto: 15 dm; Largo: 50 dm; Ancho: 40 dm y el Depósito B  Alto: 20 dm; Largo: 50 dm; Ancho: 4 dm", new List<string> { "El Depósito 1", "El Depósito 2", "El Depósito A", "El Depósito B" }, 3));
                ShuffleAndUpdate(4, "El Depósito B");
                break;
            case 6:
                trivias.Clear();

                trivias.Add(new Question("¿Cuántos turdones morados debe recolectar Murphy si desea obtener 150 g de turdolina?. Ten en cuenta que 15 g de turdolina = 100 g de turdones, 1 turdón morado = 100 g.", new List<string> { "5 turdones", "10  turdones", "15 turdones", "20 turdones" }, 1));
                ShuffleAndUpdate(0, "10 turdones");
                trivias.Add(new Question("¿Cuántos turdones naranjas debe recolectar Murphy para obtener 90 g de turdolina? Ten en cuenta que 15 g de turdolina = 100 g de turdones, 1 turdón naranja = 300 g.", new List<string> { "1 turdon", "2 turdones", "3 turdones", "4 turdones" }, 1));
                ShuffleAndUpdate(1, "2 turdones");

                trivias.Add(new Question("¿Cuántos gramos de turdolina pueden ser recolectados a partir de 30 turdones naranjas?", new List<string> { "0,35 kg", "0,35 g", "1,35 kg", "0,35 g" }, 2));
                ShuffleAndUpdate(2, "1,35 kg");

                trivias.Add(new Question("¿Cuántos gramos de turdolina pueden ser recolectados a partir de 15 turdones morados?", new List<string> { "175 g", "205 g", "225 g", "250 g" }, 0));
                ShuffleAndUpdate(3, "225 g");

                trivias.Add(new Question("¿Cuántos gramos de turdolina pueden ser recolectados a partir de 8 turdones naranjas y 2 morados?", new List<string> { "240 g", "290 g", "340 g", "390 g" }, 0));
                ShuffleAndUpdate(4, "390 g");
                break;
        }
    }
    private void JustShuffle(int index) {
        switch (index) {
            case 1:
                ShuffleAndUpdate(0, "900g");
                ShuffleAndUpdate(1, "1,5kg");
                ShuffleAndUpdate(2, "2,1kg");
                ShuffleAndUpdate(3, "6 tokens");
                ShuffleAndUpdate(4, "0,9kg");
                break;
            case 2:
                ShuffleAndUpdate(0, "21.600 metros");
                ShuffleAndUpdate(1, "59s");
                ShuffleAndUpdate(2, "50 tokens");
                ShuffleAndUpdate(3, "2 verdes");
                ShuffleAndUpdate(4, "150");
                break;
            case 3:
                ShuffleAndUpdate(0, "Utilizar la camioneta la cual se desplaza a una velocidad de 7.9 m/s");
                ShuffleAndUpdate(1, "El Unka 2 se desplaza a velocidad constante de 7.1 m/s");
                ShuffleAndUpdate(2, "25 viajes");
                ShuffleAndUpdate(3, "25 viajes");
                ShuffleAndUpdate(4, "-2,5 m/s2");
                break;
            case 4:
                ShuffleAndUpdate(0, "4 m3");
                ShuffleAndUpdate(1, "2m * 2m * 2m");
                ShuffleAndUpdate(2, "π * (1 m)^2 * 5 m");
                ShuffleAndUpdate(3, "5 π m3");
                ShuffleAndUpdate(4, "El tanque cilíndrico es más grande porque 1 π m3 es mayor a 1 m3");
                break;
            case 5:
                ShuffleAndUpdate(0, "El Depósito B");
                ShuffleAndUpdate(0, "2,35x10^13 e");
                ShuffleAndUpdate(0, "El Depósito B");
                ShuffleAndUpdate(0, "2,35x10^13 e");
                ShuffleAndUpdate(0, "El Depósito B");
                break;
            case 6:
                ShuffleAndUpdate(0, "10 turdones");
                ShuffleAndUpdate(1, "2 turdones");
                ShuffleAndUpdate(2, "1,35 kg");
                ShuffleAndUpdate(3, "225 g");
                ShuffleAndUpdate(4, "390 g");
                break;
        }
    }
    private void UpdateHUD(Question question) {
        try {
            sentence_txt.text = question.statement;
            options_txt[0].text = question.options[0];
            options_txt[1].text = question.options[1];

            if (options_txt[2].text != null) {
                options_txt[2].text = question.options[2];
            }
            if (options_txt[3].text != null) {
                options_txt[3].text = question.options[3];
            }
        } catch (Exception err) {
            Debug.Log(err.Message);
        } 
    }

    private void IntroPanel(int index) {
        intro_txt.text = "Esta es la trivia de la misión " + index + ", ayuda a Murphy a conseguir los cristales para restaurar el combistible de la nave. Puedes intentarlo cuantas veces sea necesario.";        
    }

    public void CheckAnswer() {
        if (toggles[trivias[actualQuestion].rightChoise].isOn) {
            score++;
        }
        StartCoroutine(NextAnswer());
    }

    private IEnumerator NextAnswer() {
        yield return new WaitForSeconds(1.0f);
        if (actualQuestion < trivias.Count - 1) {
            actualQuestion++;            
            UpdateHUD(trivias[actualQuestion]);
        } else {
            panelQuestions.SetActive(false);
            EndTrivia();
        }
    }
    private void EndTrivia() {
        panelResult.SetActive(true);
       if(score < 3) {
            result_img.sprite = result_sprites[0];
            result_txt.color = new Color(237,255,66);
            result_txt.text = "Puedes hacerlo mejor, tu resultado fue: " + score;
            btnResult_txt.text = "Reiniciar";
            JustShuffle(actualIndex);
        } else {
            result_img.sprite = result_sprites[1];
            result_txt.color = new Color(139, 255, 179);
            result_txt.text = "¡Muy bien! tuviste " + score+" opciones correctas.";
            btnResult_txt.text = "Finalizar trivia";
        }
    }
    public void FinalPanel() {
        if (score < 3) {
            ResetTrivia();
        } else {
            panelIntro.SetActive(true);
            panelQuestions.SetActive(false);
            panelResult.SetActive(false);
            triviaGeneralPanel.SetActive(false);
            GameManager.instance.CheckStateAndAdvance();
        }
    }
}