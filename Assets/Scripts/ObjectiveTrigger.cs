﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveTrigger : MonoBehaviour
{
    private bool contacted;
    [SerializeField] private float waitingTime;

    private void Start() {
        contacted = false;
    }
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player") && !contacted) {
            StartCoroutine(HideTriggerObject());
            contacted = true;
            switch (GameManager.instance.state) {
                case GameState.Intro:
                GameManager.instance.state = GameState.TriggerToDialCollect;
                break;
            }
        }
    }
    IEnumerator HideTriggerObject() {
        yield return new WaitForSeconds(waitingTime);
        GameManager.instance.CheckStateAndAdvance();
        gameObject.SetActive(false);
    }
}
